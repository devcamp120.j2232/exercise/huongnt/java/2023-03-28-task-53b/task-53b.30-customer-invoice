public class Customer {
    int id;
    String name;
    int discount;

    // khởi tạo phương thức
    public Customer(int id, String name, int discount) {
        this.id = id;
        this.name = name;
        this.discount = discount;
    }

    // getter

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    // in ra console
    @Override
    public String toString() {
        return String.format("(id = %s) (name =%s)(discount = %s%%)", id, name, discount);
    }

}
