import javax.print.DocPrintJob;

public class Invoice {
    int id;
    Customer customer;
    double amount;

    //khởi tạo phương thức
    public Invoice(int id, Customer customer, double amount) {
        this.id = id;
        this.customer = customer;
        this.amount = amount;
    }

    //getter

    public int getId() {
        return id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public double getAmount() {
        return amount;
    }

    public int getCustomerId() {
        return customer.id;
    }

    public String getCustomerName(){
        return customer.name;
    }

    public int getCustomerDiscount() {
        return customer.discount;
    }

    public double getAmountAfterDiscount(){
        int dis = getCustomerDiscount();
        amount = this.amount - ((this.amount * dis)/100);
        return amount;
    } 


    //setter
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }


           // in ra console
           @Override
           public String toString() {
               return String.format("Invoice[id =" + this.id + ", Customer[name = " + this.customer.getName() + ", discount = " + this.customer.getDiscount() + "], amount = " + getAmountAfterDiscount()) + "]";
           }

    
    
    
    
}
