public class App {
    public static void main(String[] args) throws Exception {
                // khởi tạo đối tượng customer1
                Customer customer1 = new Customer(1, "Huong Nguyen", 10);
                // khởi tạo đối tượng customer2
                Customer customer2 = new Customer(2, "Nguyen Tran", 20);
        
                // in ra console
                System.out.println("Customer 1");
                System.out.println(customer1.toString());
                System.out.println("Customer 2");
                System.out.println(customer2.toString());
        
                // khởi tạo đối tượng book1
                Invoice invoice1 = new Invoice(1, customer1, 200000);
                // khởi tạo đối tượng book2
                Invoice invoice2 = new Invoice(2, customer2, 250000);
        
                System.out.println("Invoice 1");
                System.out.println(invoice1.toString());
                System.out.println("Invoice 2");
                System.out.println(invoice2.toString());

    }
}
